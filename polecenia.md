
# GIT
cd ..
git clone https://ev45ive@bitbucket.org/ev45ive/altkom-react-open-luty.git altkom-react-open-luty
cd altkom-react-open-luty
npm i 


# Intalacja globalna CRA
https://pl.reactjs.org/docs/create-a-new-react-app.html

npm i -g create-react-app 

create-react-app --help

create-react-app . --template typescript


# Boostrap css 
yarn add bootstrap
npm install bootstrap

import 'bootstrap/dist/css/bootstrap.css';

## VSCODE 
https://marketplace.visualstudio.com/items?itemName=dsznajder.es7-react-js-snippets

## Chrome
https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi

## classNames helper
https://github.com/JedWatson/classnames

# Komunikacja http
npm i axios
yarn add axios

https://swr.vercel.app/ 

# Router
npm install react-router-dom
yarn add react-router-dom

npm i --save-dev @types/react-router-dom
yarn add -D @types/react-router-dom

# Redux
npm i redux react-redux @types/react-redux
yarn add redux react-redux @types/react-redux


## Middleware
 yarn add redux-logger @types/redux-logger redux-thunk redux-promise-middleware

 https://github.com/zalmoxisus/redux-devtools-extension


## Build
npm run build 

  https://cra.link/deployment



## API 
```ts
await (await fetch('https://www.googleapis.com/books/v1/volumes?q=flowers+inauthor:keyes&key= KLUZC ')).json()


await (await fetch('https://api.themoviedb.org/3/search/movie?api_key= KLUC &query=alice')).json()



var client_id = 'adasdasd';
var client_secret = 'asdasdasd';
await (await fetch('https://accounts.spotify.com/api/token',{
    method:'POST',
    headers:{
        'Content-Type':'application/x-www-form-urlencoded',
        'Authorization': `Basic ${btoa(`${client_id}:${client_secret}`)}`
    },
    body: new URLSearchParams({ grant_type: 'client_credentials' }).toString()

})).json()



```