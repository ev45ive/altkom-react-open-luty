import { Reducer } from 'redux';

interface INCREMENT {
  type: 'INCREMENT'; payload: number;
}
interface DECREMENT {
  type: 'DECREMENT'; payload: number;
}
interface RESET { type: 'RESET'; payload: number; }

type Action = INCREMENT | DECREMENT | RESET;

export const featureKey = 'counter';

export const counterReducer: Reducer<number, Action> = (state = 0, action) => {
  // if(action.specialKey !== 'costam'){ return state}

  switch (action.type) {
    case 'INCREMENT': return state + action.payload;
    case 'DECREMENT': return state - action.payload;
    case 'RESET': return action.payload;
    default: return state;
  }
};


export const inc = (payload = 1): INCREMENT => ({ type: 'INCREMENT', payload })
export const dec = (payload = 1): DECREMENT => ({ type: 'DECREMENT', payload })