import axios from 'axios'
import React, { FC, useEffect, useState } from 'react'
import { UserProfile } from '../model/UserProfile'
import { auth } from '../services'

interface UserCtx {
  user: UserProfile | null;
  login(): void
  logout(): void
}

export const UserContext = React.createContext<UserCtx>({
  user: null,
  login() { },
  logout() { }
})


export const UserContextProvider: FC = ({ children }) => {
  const [user, setUser] = useState<UserProfile | null>(null)

  const login = () => {
    axios.get<UserProfile>('https://api.spotify.com/v1/me').then(res => {
      setUser(res.data)
    })
  }
  const logout = () => { setUser(null); auth.login() }

  useEffect(() => {
    if (!user) { login() }
  }, [])

  return (
    <UserContext.Provider value={{ user, login, logout }}>
      {children}
    </UserContext.Provider>
  )
}


type Props = {} & React.DetailedHTMLProps<React.HTMLAttributes<HTMLElement>, HTMLElement>

export const UserWidget: FC<Props> = ({ className, ...restProps }) => {
  return (
    <UserContext.Consumer>{({ user, login, logout }: UserCtx) =>
      <span {...restProps} className={"navbar-text " + className}>

        {user && <span>Welcome, {user.display_name} |
          <a onClick={logout}> Logout</a>
        </span>}

        {!user && <span>Welcome, Guest |
          <a onClick={login}> Login</a>
        </span>}
      </span>
    }</UserContext.Consumer>
  )
}
