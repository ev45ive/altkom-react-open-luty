import { useState, useEffect } from "react"

export function useDataFetchService<T>(fetch: () => Promise<T>, deps: any[] = []) {

  const [results, setResults] = useState<T>()
  const [message, setMessage] = useState('')
  const [isLoading, setIsLoading] = useState(false)

  useEffect(() => {
    setMessage('')
    setIsLoading(true)

    fetch().then(data => setResults(data))
      .catch(error => setMessage(error.message))
      .finally(() => setIsLoading(false))
  }, deps)

  return { message, isLoading, data: results }
}
