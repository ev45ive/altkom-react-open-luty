import React from 'react'
import { Album } from '../../core/model/Album'

import { Link, useHistory } from 'react-router-dom'

interface Props {
  result: Album
}

export const AlbumCard = ({ result }: Props) => {


  const { push } = useHistory()

  return (
    <div className="card" onClick={() => push(`/albums/${result.id}`)}>
      {result.images[0] && <img src={result.images[0].url} className="card-img-top" alt={result.name} />}
      <div className="card-body">
        <Link to={`/albums/${result.id}`}>
          <h5 className="card-title">{result.name}</h5>
        </Link>
      </div>
    </div>
  )
}
