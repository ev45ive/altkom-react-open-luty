import React, { useEffect, useRef, useState } from 'react'

interface Props {
  query: string
  onSearch(query: string): void
}
export const SearchForm = ({ onSearch, query: parentQuery }: Props) => {

  const [query, setQuery] = useState(parentQuery)
  const inputRef = useRef<HTMLInputElement>(null)
  const isFirst = useRef(true)

  useEffect(() => {
    isFirst.current = true
    setQuery(parentQuery)
  }, [parentQuery])

  useEffect(() => {
    inputRef.current?.focus()
  }, [])

  useEffect(() => {
    if (isFirst.current) { isFirst.current = (false); return }

    const handler = setTimeout(() => {
      onSearch(query)
    }, 400)

    return () => clearTimeout(handler)
  }, [query])

  return (
    <div>
      <div className="input-group mb-3">

        <input type="text" className="form-control" placeholder="Search" ref={inputRef}
          value={query} onChange={e => setQuery(e.target.value)} />

        <div className="input-group-append">
          <button className="btn btn-outline-secondary" type="button" onClick={() => onSearch(query)}>Search</button>
        </div>
      </div>
    </div>
  )
}
