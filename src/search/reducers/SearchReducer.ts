import { Album } from "../../core/model/Album"

export const initialState = {
  query: '',
  results: [] as Album[],
  message: '',
  isLoading: false
}

type SEARCH_START = { type: 'SEARCH_START', payload: { query: string } }
type SEARCH_SUCCESS = { type: 'SEARCH_SUCCESS', payload: { results: Album[] } }
type SEARCH_FAILED = { type: 'SEARCH_FAILED', payload: { error: Error } }
type Actions = SEARCH_START | SEARCH_SUCCESS | SEARCH_FAILED

export const reducer = (state = initialState, action: Actions): typeof initialState => {
  switch (action.type) {
    case 'SEARCH_START': return {
      ...state,
      isLoading: true,
      message: '',
      results: [],
      query: action.payload.query
    }
    case 'SEARCH_SUCCESS': return {
      ...state,
      isLoading: false,
      results: action.payload.results
    }
    case 'SEARCH_FAILED': return {
      ...state,
      isLoading: false,
      message: action.payload.error.message
    }
    default: return state
  }
}

export const searchStart = (payload: { query: string }): SEARCH_START => ({ type: 'SEARCH_START', payload })
export const searchSuccess = (payload: { results: Album[] }): SEARCH_SUCCESS => ({ type: 'SEARCH_SUCCESS', payload })
export const searchFailed = (payload: { error: Error }): SEARCH_FAILED => ({ type: 'SEARCH_FAILED', payload })
