import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { auth } from './core/services';

// import { HashRouter as Router } from 'react-router-dom'
import { BrowserRouter as Router } from 'react-router-dom'
import { UserContextProvider } from './core/contexts/UserContext';
import { store } from './store';
import { Provider } from 'react-redux';

store.getState()

auth.init()

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <UserContextProvider>
        <Router>
          <App />
        </Router>
      </UserContextProvider>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// // If you want to start measuring performance in your app, pass a function
// // to log results (for example: reportWebVitals(console.log))
// // or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();


// window.React = React
// window.ReactDOM = ReactDOM

// const PersonSpan = (props: { name: string, pet: string }) => React.createElement('span', {}, props.name + ' ma ' + props.pet);
// const PersonSpan = (props: { name: string, pet: string }) => <span>{props.name + ' ma ' + props.pet}</span>

// let i = 0;
// // setInterval(() => {

// const div1 = React.createElement('p', {
//   id: 'test',
//   style: { color: 'red', backgroundColor: 'lightblue' },
//   className: 'naszaklasa'
// },
//   // React.createElement('span', {}, 'ala ma kota'),
//   PersonSpan({ name: 'Ala', pet: 'kota' }),
//   PersonSpan({ name: 'TOm', pet: 'psa' }),
//   PersonSpan({ name: 'BOb', pet: 'samolot' }),
//   React.createElement('input', {}),
//   React.createElement('span', {}, 'i iguane ' + i++),
// )

// debugger

// const div2 = <div>
//   <p id="test" style={{ color: 'red', backgroundColor: 'lightblue' }} className={"test" + i}>
//     ALa ma {i} koty
//     {PersonSpan({ name: 'Bob', pet: 'papugi' })}

//     <PersonSpan name="Alice" pet="rybki"></PersonSpan>
//     <PersonSpan name="Tom" pet="psa"></PersonSpan>

//     <input type="text" />
//   </p>
// </div>;

// ReactDOM.render(div, document.getElementById('root'))

// }, 1000)
