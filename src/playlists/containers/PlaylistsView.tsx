
// tsrafc

import React, { useContext, useEffect, useState } from 'react'
import { PlaylistDetails } from '../components/PlaylistDetails'
import { Playlist } from "../../core/model/Playlist"
import { PlaylistEditForm } from '../components/PlaylistEditForm'
import { PlaylistList } from '../components/PlaylistList'
import { UserContext } from '../../core/contexts/UserContext'


const examplePlaylists: Playlist[] = [
  
]


interface Props {

}

export const PlaylistsView = (props: Props) => {
  const [playlists, setPlaylists] = useState(examplePlaylists)
  const [selectedId, setSelectedId] = useState<Playlist['id'] | null>(null)
  const [selected, setSelected] = useState<Playlist | undefined>()
  const [mode, setMode] = useState<'details' | 'edit'>('details')

  const { user } = useContext(UserContext)

  const edit = () => { setMode('edit') }
  const cancel = () => { setMode('details') }

  const save = (draft: Playlist) => {
    setPlaylists(playlists.map(p => p.id === draft.id ? draft : p))
    setMode('details')
  }

  const select = (id: Playlist['id']) => {
    setSelectedId(id)
  }

  useEffect(() => {
    setSelected(playlists.find(p => p.id === selectedId))
  }, [selectedId, playlists])

  return (
    <div>
      <div className="row">

        <div className="col">
          <PlaylistList playlists={playlists} selectedId={selectedId}
            onSelect={select} />
        </div>

        <div className="col">
          {!selected && <p>Please select playlist</p>}

          {selected && <>

            {mode === 'details' ? <div>
              <PlaylistDetails playlist={selected} onEdit={edit} />
            </div> : null}

            {mode === 'edit' && <div>
              <PlaylistEditForm playlist={selected} onSave={save} onCancel={cancel} />
            </div>}

          </>}

        </div>

      </div>

    </div>
  )
}
